# SVG Heraldic Charges

These are SVG traces of various public domain heraldic images.

Since it's public domain, go ahead and use this for whatever you want. I have no claim to any of it.
